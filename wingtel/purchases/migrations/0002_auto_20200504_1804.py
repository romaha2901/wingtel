# Generated by Django 2.2.1 on 2020-05-04 18:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('purchases', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='purchase',
            name='amount',
        ),
        migrations.RemoveField(
            model_name='purchase',
            name='updated_at',
        ),
        migrations.RemoveField(
            model_name='purchase',
            name='user',
        ),
    ]
