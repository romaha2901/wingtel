from rest_framework import serializers
from wingtel.purchases.models import Purchase


class PurchaseSerializer(serializers.ModelSerializer):

    # add readable appropriate model name instead of id's
    subscription = serializers.ReadOnlyField(source='content_type.model')

    class Meta:
        model = Purchase
        fields = '__all__'
