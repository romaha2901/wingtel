from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from model_utils import Choices


class Purchase(models.Model):
    """Represents a purchase for a user and their subscription(s)"""
    STATUS = Choices(
        ('pending', 'Pending'),
        ('overdue', 'Past Due'),
        ('complete', 'Complete'),
        ('cancelled', 'Cancelled'),
    )

    # https://docs.djangoproject.com/en/2.2/ref/contrib/contenttypes/
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    subscription_content_object = GenericForeignKey('content_type', 'object_id')

    status = models.CharField(max_length=20, choices=STATUS, default=STATUS.pending)
    payment_date = models.DateTimeField(null=True, db_index=True)

    created_at = models.DateTimeField(auto_now_add=True)
