from rest_framework import viewsets

from wingtel.purchases.models import Purchase
from wingtel.purchases.serializers import PurchaseSerializer


class PurchaseViewSet(viewsets.ReadOnlyModelViewSet):
    """
        A view set that provides `retrieve`, and `list` actions.
        As we would like to manually control purchases.
    """
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer

