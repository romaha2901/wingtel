from django.apps import AppConfig


class SubscriptionsConfig(AppConfig):
    name = 'wingtel.subscriptions'
    label = 'subscriptions'

    def ready(self):
        from wingtel.subscriptions.signals import subscription_activated, create_purchase
        subscription_activated.connect(create_purchase)

