from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied

from wingtel.subscriptions.serializers import ATTSubscriptionSerializer, SprintSubscriptionSerializer
from wingtel.subscriptions.models import ATTSubscription, SprintSubscription
from wingtel.subscriptions.permissions import IsOwner
from wingtel.subscriptions.signals import subscription_activated


class ModelViewSetWithActiveSubscription(ModelViewSet):

    @action(detail=True, methods=['POST'], permission_classes=[IsOwner])
    def activate(self, request, pk=None):
        subscription = self.get_object()
        if subscription.status == 'new':
            subscription.status = 'active'
            subscription.save()
            # send a signal to create a purchase, based on subscription
            subscription_activated.send(sender=self.__class__,  instance=subscription)
            return Response(self.get_serializer(subscription).data)
        raise PermissionDenied('"activate" operation is permitted only for subscriptions with status "new".')


class ATTSubscriptionViewSet(ModelViewSetWithActiveSubscription):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions. And activate "new" subscription
    """
    queryset = ATTSubscription.objects.all()
    serializer_class = ATTSubscriptionSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class SprintSubscriptionViewSet(ModelViewSetWithActiveSubscription):
    """
    A viewset that provides `retrieve`, `create`, and `list` actions. And activate "new" subscription
    """
    queryset = SprintSubscription.objects.all()
    serializer_class = SprintSubscriptionSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


