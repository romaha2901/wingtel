import django.dispatch
from django.utils import timezone

from wingtel.purchases.models import Purchase


subscription_activated = django.dispatch.Signal(providing_args=['instance'])


def create_purchase(sender, **kwargs):
    """ Signal handler to create Purchase object due to a subscription plan """
    subscription = kwargs['instance']
    Purchase.objects.create(
        user=subscription.user,
        subscription_content_object=subscription,
        status=Purchase.STATUS.overdue,
        amount=subscription.plan.price,
        payment_date=timezone.now()
    )
