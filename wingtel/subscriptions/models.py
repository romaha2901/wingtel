from model_utils import Choices

from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericRelation

from wingtel.plans.models import Plan
from wingtel.purchases.models import Purchase


class BaseSubscription(models.Model):
    """Represents a base subscription for a user and a single device. Stores common logic"""

    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)  # Owning user
    plan = models.ForeignKey(Plan, null=True, on_delete=models.PROTECT)

    device_id = models.CharField(max_length=20)
    phone_number = models.CharField(max_length=20)
    phone_model = models.CharField(max_length=128)

    effective_date = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def __str__(self):
        return f'{self.user.username} - {self.plan}'


class SprintSubscription(BaseSubscription):
    """Represents a subscription with Sprint for a user and a single device"""

    STATUS = Choices(
        ('new', 'New'),
        ('active', 'Active'),
        ('suspended', 'Suspended'),
        ('expired', 'Expired'),
    )
    sprint_id = models.CharField(max_length=16, null=True)
    status = models.CharField(max_length=10, choices=STATUS, default=STATUS.new)
    # https://docs.djangoproject.com/en/2.2/ref/contrib/contenttypes/
    purchases = GenericRelation(Purchase, related_query_name='sprint_subscription')


class ATTSubscription(BaseSubscription):
    """Represents a subscription with AT&T for a user and a single device"""
    
    STATUS = Choices(
        ('new', 'New'),
        ('active', 'Active'),
        ('expired', 'Expired'),
    )
    network_type = models.CharField(max_length=5, blank=True, default='')
    status = models.CharField(max_length=10, choices=STATUS, default=STATUS.new)
    # https://docs.djangoproject.com/en/2.2/ref/contrib/contenttypes/
    purchases = GenericRelation(Purchase, related_query_name='att_subscription')


