from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    """ Object-level permissions for any operations for owning object """
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
